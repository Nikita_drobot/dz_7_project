from django.conf import settings
from django.contrib import admin  # NOQA: F401
from django.core.mail import send_mail
from django.utils.html import format_html

from .models import Student
from .utils.filters import AgeListFilter


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    ordering = (
        "first_name",
        "group__name",
    )

    list_display = (
        "full_name",
        "email",
        "grade",
        "age",
        "view_birth_date",
        "view_resume",
        "group",
    )

    list_editable = (
        "grade",
        "group",
    )

    list_display_links = (
        "full_name",
        "email",
    )

    date_hierarchy = "birth_date"

    actions = ["send_email"]

    list_filter = (
        "grade",
        "group__name",
        AgeListFilter,
    )

    raw_id_fields = ("group",)

    search_fields = (
        "first_name__istartswith",
        "email",
    )

    @admin.display(empty_value="Unknown date")
    def view_birth_date(self, obj):
        return obj.birth_date

    @admin.display(description="Full name")
    def full_name(self, obj):
        return ("%s %s" % (obj.first_name, obj.last_name)).title()

    @admin.display(description="Resume")
    def view_resume(self, obj):
        if obj.resume:
            return format_html('<a href="{}" target="_blank">View Resume</a>', obj.resume.url)
        return "No resume"

    @admin.display(description="Send email to selected students")
    def send_email(self, request, queryset):
        subject = "Reminder Message"
        message = "Hi, please do your homework, the deadline will end soon!"
        for student in queryset:
            send_mail(subject, message, settings.EMAIL_HOST_USER, [student.email])
        self.message_user(request, f"Email sent to {queryset.count()} students.")
