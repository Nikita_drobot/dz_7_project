import datetime

from django.contrib import admin  # NOQA: F401
from django.utils.translation import gettext_lazy as _


class AgeListFilter(admin.SimpleListFilter):
    title = _("Age")
    parameter_name = "age"

    def lookups(self, request, model_admin):
        return (
            ("less_20", _("Under 20 years old")),
            ("20_and_above", _("20 years old and above")),
        )

    def queryset(self, request, queryset):
        today = datetime.datetime.now()
        birth_date = today - datetime.timedelta(days=20 * 365)
        if self.value() == "less_20":
            return queryset.filter(birth_date__gte=birth_date)

        elif self.value() == "20_and_above":
            return queryset.filter(birth_date__lt=birth_date)

        return queryset
