import datetime
import random
from uuid import uuid4

from django.db import models
from django.utils.translation import gettext_lazy as _
from faker import Faker

from groups.models import Group
from main.models import Person


class Student(Person):
    uuid = models.UUIDField(default=uuid4, primary_key=True, editable=False, unique=True, db_index=True)
    avatar = models.ImageField(upload_to="images/students", blank=True, null=True)
    grade = models.PositiveSmallIntegerField(default=0, null=True)
    birth_date = models.DateField(null=True)
    group = models.ForeignKey(to=Group, null=True, on_delete=models.CASCADE)
    resume = models.FileField(upload_to="resumes/", blank=True, null=True)

    class Meta:
        verbose_name = _("Student")
        verbose_name_plural = _("All students")

    def age(self):
        try:
            return datetime.datetime.now().year - self.birth_date.year
        except AttributeError:
            return None

    def __str__(self):
        return f"{self.first_name}_{self.last_name} {self.email}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for i in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                grade=random.randint(1, 100),
                birth_date=faker.date_time_between(start_date="-40y", end_date="-18y"),
                group_id=1,
            )
