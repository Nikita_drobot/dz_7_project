from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import (
    CreateView,
    UpdateView,
    DeleteView,
    ListView,
)

from students.forms import StudentForm
from students.models import Student


class StudentListView(LoginRequiredMixin, ListView):
    model = Student
    template_name = "students/list.html"
    context_object_name = "students"

    def get_queryset(self):
        queryset = super().get_queryset()
        search_text = self.request.GET.get("search_text", "")

        if search_text:
            search_fields = [
                "last_name__icontains",
                "first_name__icontains",
                "email__icontains",
            ]
            or_filter = Q()

            for field in search_fields:
                or_filter |= Q(**{field: search_text})

            queryset = queryset.filter(or_filter)

        return queryset


class CreateStudentView(LoginRequiredMixin, CreateView):
    template_name = "students/create.html"
    form_class = StudentForm
    initial = {"grade": "50"}
    success_url = reverse_lazy("students:get_students")


class UpdateStudentView(UpdateView):
    template_name = "students/update.html"
    form_class = StudentForm
    pk_url_kwarg = "uuid"
    queryset = Student.objects.all()
    success_url = reverse_lazy("students:get_students")


class DeleteStudentView(DeleteView):
    template_name = "students/delete.html"
    form_class = StudentForm
    pk_url_kwarg = "uuid"
    queryset = Student.objects.all()
    success_url = reverse_lazy("students:get_students")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = StudentForm(instance=self.object)
        return context
