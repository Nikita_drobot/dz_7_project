from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
    DetailView,
)

from groups.models import Group
from teachers.forms import TeacherForm
from teachers.models import Teacher


class TeacherListView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = "teachers/list.html"
    context_object_name = "teachers"

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset


class CreateTeacherView(LoginRequiredMixin, CreateView):
    template_name = "teachers/create.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")


class UpdateTeachersView(UpdateView):
    template_name = "teachers/update.html"
    form_class = TeacherForm
    pk_url_kwarg = "pk"
    queryset = Teacher.objects.all()
    success_url = reverse_lazy("teachers:get_teachers")


class DeleteTeacherView(DeleteView):
    template_name = "teachers/delete.html"
    form_class = TeacherForm
    pk_url_kwarg = "pk"
    queryset = Teacher.objects.all()
    success_url = reverse_lazy("teachers:get_teachers")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = TeacherForm(instance=self.object)
        return context


class GroupDetailsView(DetailView):
    model = Group
    template_name = "teachers/groups.html"
    context_object_name = "group"

    def get_object(self, **kwargs):
        return get_object_or_404(Group.objects.all(), pk=self.kwargs["pk"])
