from django.contrib import admin  # NOQA: F401
from django.urls import reverse
from django.utils.html import format_html

from .models import Teacher
from .utils.filters import WorkExperienceListFilter


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    ordering = ("first_name",)

    list_display = (
        "full_name",
        "email",
        "age",
        "work_experience",
        "english_level",
        "subject",
        "group_display",
    )

    list_editable = ("work_experience",)

    list_display_links = (
        "full_name",
        "email",
    )

    list_filter = (
        "subject",
        "english_level",
        WorkExperienceListFilter,
    )

    search_fields = (
        "first_name__istartswith",
        "email",
    )

    @admin.display(description="Full name")
    def full_name(self, obj):
        return ("%s %s" % (obj.first_name, obj.last_name)).title()

    @admin.display(description="Group")
    def group_display(self, obj):
        info = format_html(
            ", ".join(
                [
                    '<a href="{}">{}</a>'.format(
                        reverse(
                            "admin:{}_{}_change".format(group._meta.app_label, group._meta.model_name),
                            args=[group.id],
                        ),
                        group.name,
                    )
                    for group in obj.group.all()
                ]
            )
        )
        if info:
            return info
        else:
            return "Group not specified"
