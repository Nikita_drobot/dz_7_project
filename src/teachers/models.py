from random import randint

from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from faker import Faker

from groups.models import Group
from main.models import Person
from main.utils.validators import subject_validator


class Teacher(Person):
    avatar = models.ImageField(upload_to="images/teachers", blank=True, null=True)
    age = models.PositiveSmallIntegerField(null=True, validators=[MinValueValidator(15)])
    work_experience = models.PositiveSmallIntegerField(null=True)
    english_level = models.BooleanField(null=True)
    subject = models.CharField(max_length=100, blank=True, null=True, validators=[subject_validator])
    group = models.ManyToManyField(
        to=Group,
    )

    class Meta:
        verbose_name = _("Teacher")
        verbose_name_plural = _("All teachers")

    def __str__(self):
        return f"{self.first_name}_{self.last_name} {self.email}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for i in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                age=randint(20, 100),
                work_experience=randint(1, 20),
                english_level=faker.boolean(chance_of_getting_true=50),
                subject=faker.word(
                    ext_word_list=[
                        "Math",
                        "Chemistry",
                        "Music",
                        "Physics",
                        "History",
                        "IT",
                    ]
                ),
            )
