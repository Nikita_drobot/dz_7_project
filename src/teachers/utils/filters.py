from django.contrib import admin  # NOQA: F401
from django.utils.translation import gettext_lazy as _


class WorkExperienceListFilter(admin.SimpleListFilter):
    title = _("Work experience")
    parameter_name = "work_experience"

    def lookups(self, request, model_admin):
        return (
            ("less_10", _("Under 10 years of experience")),
            ("10_and_above", _("10 years of experience and above")),
        )

    def queryset(self, request, queryset):
        if self.value() == "less_10":
            return queryset.filter(work_experience__lt=10)

        elif self.value() == "10_and_above":
            return queryset.filter(work_experience__gte=10)

        return queryset
