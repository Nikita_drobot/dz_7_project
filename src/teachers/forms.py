from django.core.exceptions import ValidationError
from django.forms import ModelForm

from teachers.models import Teacher


class TeacherForm(ModelForm):
    class Meta:
        model = Teacher
        fields = (
            "avatar",
            "first_name",
            "last_name",
            "email",
            "age",
            "work_experience",
            "english_level",
            "subject",
            "group",
        )

    @staticmethod
    def normal_text(text: str):
        return text.strip().capitalize()

    def clean_first_name(self):
        return self.normal_text(self.cleaned_data["first_name"])

    def clean_last_name(self):
        return self.normal_text(self.cleaned_data["last_name"])

    def clean(self):
        cleaned_data = super().clean()

        age = cleaned_data["age"]
        work_experience = cleaned_data["work_experience"]

        if work_experience >= age - 10:
            raise ValidationError("Work_experience must be at least 10 units less than your age")

        return cleaned_data
