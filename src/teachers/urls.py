from django.urls import path

from teachers.views import (
    TeacherListView,
    CreateTeacherView,
    UpdateTeachersView,
    DeleteTeacherView,
    GroupDetailsView,
)

app_name = "teachers"

urlpatterns = [
    path("", TeacherListView.as_view(), name="get_teachers"),
    path("create", CreateTeacherView.as_view(), name="create_teacher"),
    path("update/<int:pk>/", UpdateTeachersView.as_view(), name="update_teacher"),
    path("delete/<int:pk>/", DeleteTeacherView.as_view(), name="delete_teacher"),
    path("group-details/<int:pk>/", GroupDetailsView.as_view(), name="group_details"),
]
