from django.core.exceptions import ValidationError
from django.forms import ModelForm

from groups.models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = (
            "name",
            "number",
            "students_count",
            "start_date",
            "subject",
            "description",
        )

    def clean_students_count(self):
        students_count = self.cleaned_data["students_count"]
        if students_count > 30:
            raise ValidationError("The number of students in a group cannot be more than 30")
        return students_count
