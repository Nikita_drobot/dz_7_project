from django.contrib import admin  # NOQA: F401
from django.utils.translation import gettext_lazy as _


class NumberListFilter(admin.SimpleListFilter):
    title = _("Number of group")
    parameter_name = "number"

    def lookups(self, request, model_admin):
        return (
            ("less_100", _("Numbers less than 100")),
            ("between_100_and_200", _("Numbers between 100 and 200")),
            ("200_and_above", _("Numbers greater than 200")),
        )

    def queryset(self, request, queryset):
        if self.value() == "less_100":
            return queryset.filter(number__lt=100)

        elif self.value() == "between_100_and_200":
            return queryset.filter(number__range=(100, 200))

        elif self.value() == "200_and_above":
            return queryset.filter(number__gte=200)

        return queryset


class StudentCountListFilter(admin.SimpleListFilter):
    title = _("Student count")
    parameter_name = "students_count"

    def lookups(self, request, model_admin):
        return (
            ("fully_groups", _("Fully filled groups")),
            ("incomplete_groups", _("Incomplete groups")),
        )

    def queryset(self, request, queryset):
        if self.value() == "fully_groups":
            return queryset.filter(students_count=30)

        elif self.value() == "incomplete_groups":
            return queryset.filter(students_count__lt=30)

        return queryset
