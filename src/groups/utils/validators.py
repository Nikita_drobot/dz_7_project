from datetime import datetime, timedelta

from django.core.exceptions import ValidationError


def start_date_validator(start_date) -> None:
    if start_date > datetime.now().date() + timedelta(days=365):
        raise ValidationError(
            "The start date of the course must be less than today's date" " by adding another year to this date"
        )


def student_count_validator(student_count) -> None:
    if student_count > 30:
        raise ValidationError("The maximum number of students in one group is 30 people!")
