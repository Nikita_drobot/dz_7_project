from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from groups.forms import GroupForm
from groups.models import Group


class GroupListView(LoginRequiredMixin, ListView):
    model = Group
    template_name = "groups/list.html"
    context_object_name = "groups"

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset


class CreateGroupView(LoginRequiredMixin, CreateView):
    template_name = "groups/create.html"
    form_class = GroupForm
    initial = {"start_date": "2007-10-25"}
    success_url = reverse_lazy("groups:get_groups")


class UpdateGroupView(UpdateView):
    template_name = "groups/update.html"
    form_class = GroupForm
    pk_url_kwarg = "pk"
    queryset = Group.objects.all()
    success_url = reverse_lazy("groups:get_groups")


class DeleteGroupView(DeleteView):
    template_name = "groups/delete.html"
    form_class = GroupForm
    pk_url_kwarg = "pk"
    queryset = Group.objects.all()
    success_url = reverse_lazy("groups:get_groups")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = GroupForm(instance=self.object)
        return context
