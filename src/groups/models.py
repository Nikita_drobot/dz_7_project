from random import randint

from django.db import models
from django.utils.translation import gettext_lazy as _
from faker import Faker

from groups.utils.validators import start_date_validator, student_count_validator
from main.utils.validators import subject_validator


class Group(models.Model):
    name = models.CharField(max_length=200, null=True)
    number = models.PositiveSmallIntegerField()
    students_count = models.PositiveSmallIntegerField(validators=[student_count_validator])
    start_date = models.DateField(null=True, validators=[start_date_validator])
    subject = models.CharField(max_length=100, blank=True, null=True, validators=[subject_validator])
    description = models.TextField(null=True)

    class Meta:
        verbose_name = _("Group")
        verbose_name_plural = _("All groups")

    def __str__(self):
        return f"{self.name} ({self.id})"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for i in range(count):
            cls.objects.create(
                name=faker.company(),
                number=randint(1, 300),
                students_count=randint(10, 30),
                start_date=faker.date_this_century(),
                subject=faker.word(
                    ext_word_list=[
                        "Math",
                        "Chemistry",
                        "Music",
                        "Physics",
                        "History",
                        "IT",
                    ]
                ),
                description=faker.sentence(nb_words=10),
            )
