from django.contrib import admin  # NOQA: F401

from .models import Group
from .utils.filters import NumberListFilter, StudentCountListFilter


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    ordering = (
        "name",
        "start_date",
    )

    list_display = (
        "name",
        "number",
        "students_count",
        "start_date",
        "subject",
    )

    list_editable = ("students_count",)

    date_hierarchy = "start_date"
    list_filter = (
        "subject",
        NumberListFilter,
        StudentCountListFilter,
    )

    search_fields = ("name__istartswith",)
