from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = [
            "email",
            "phone_number",
            "password1",
            "password2",
            "first_name",
            "last_name",
        ]

    def clean(self):
        _clean_data = super().clean()

        if not bool(self.cleaned_data["email"]) and not bool(self.cleaned_data["phone_number"]):
            raise ValidationError("Insert email or phone number. At least one of them should be set.")

        elif not bool(self.cleaned_data["email"]):
            if get_user_model().objects.filter(phone_number=_clean_data["phone_number"]).exists():
                raise ValidationError("User with phone number already exists!!!")

        elif not bool(self.cleaned_data["phone_number"]):
            if get_user_model().objects.filter(email=_clean_data["email"]).exists():
                raise ValidationError("User with email already exists!!!")

        return _clean_data
