from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import MinLengthValidator, RegexValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from main.managers import CustomerManager
from students.utils.validators import first_name_validator


class Customer(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("Name"), max_length=150, blank=True)
    last_name = models.CharField(_("Surname"), max_length=150, blank=True)
    phone_number = PhoneNumberField(
        _("phone_number"),
        null=True,
        blank=True,
        validators=[
            RegexValidator(
                regex=r"^\+?1?\d{9,15}$",
                message="Phone number must be entered in the format: '+666666666'. Up to 15 digits allowed.",
            )
        ],
    )
    email = models.EmailField(_("email address"), null=True, blank=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    department = models.CharField(_("department"), max_length=300, null=True, blank=True)
    birth_date = models.DateTimeField(_("birth_date"), null=True, blank=True)
    avatar = models.ImageField(_("avatar"), upload_to="images/profiles", null=True, blank=True)

    objects = CustomerManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("customer")
        verbose_name_plural = _("customers")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def get_period_of_registration(self):
        return f"Time on site: {timezone.now() - self.date_joined}"

    def __str__(self):
        return f"{self.phone_number}"


class UserProfile(models.Model):
    TYPE_CHOICES = [
        ("Teacher", "Teacher"),
        ("Student", "Student"),
        ("Mentor", "Mentor"),
    ]
    GENDER_CHOICES = [
        ("M", "Male"),
        ("F", "Female"),
        ("O", "Another"),
    ]
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    # photo = models.ImageField(upload_to="images/profiles", blank=True, null=True)
    # phone_number = PhoneNumberField(blank=True)
    user_type = models.CharField(max_length=50, choices=TYPE_CHOICES, blank=True, null=True)
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES, blank=True, null=True)
    instagram_url = models.URLField(max_length=200, blank=True, null=True)
    city_name = models.CharField(max_length=100, blank=True, default="Dnipro")
    latitude = models.FloatField(blank=True, default=48)
    longitude = models.FloatField(blank=True, default=35)

    def __str__(self):
        return f"{self.user.first_name}_{self.user.last_name} {self.user.phone_number}"


class Person(models.Model):
    first_name = models.CharField(
        max_length=150,
        blank=True,
        null=True,
        validators=[MinLengthValidator(3), first_name_validator],
    )
    last_name = models.CharField(max_length=150, blank=True, null=True)
    email = models.EmailField(max_length=150)

    class Meta:
        abstract = True
