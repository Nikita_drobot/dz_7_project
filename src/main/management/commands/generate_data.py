from django.core.management.base import BaseCommand

from groups.models import Group
from students.models import Student
from teachers.models import Teacher


class Command(BaseCommand):
    help = "Generate 20 records for students and teachers."

    def handle(self, *args, **kwargs):
        Group.generate_instances(20)
        Student.generate_instances(20)
        Teacher.generate_instances(20)
        self.stdout.write("Generating was successful")
