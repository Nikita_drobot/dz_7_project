from django.contrib import admin  # NOQA: F401
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.utils.html import format_html

from .models import UserProfile


class ProfileAdminInline(admin.StackedInline):
    model = UserProfile


@admin.register(get_user_model())
class CustomerAdmin(UserAdmin):
    inlines = [ProfileAdminInline]
    ordering = ("email",)
    list_display = ("email", "first_name", "last_name")
    fieldsets = None
    readonly_fields = ("email",)


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    ordering = ("user__email",)

    list_display = (
        "user_email",
        "user_phone_number",
        "user_type",
        "gender",
        "instagram",
        "city_name",
    )

    list_editable = (
        "user_type",
        "gender",
    )

    list_display_links = ("user_email",)

    list_filter = (
        "user_type",
        "gender",
    )

    search_fields = ("user__email",)

    @admin.display(description="Email")
    def user_email(self, obj):
        info_email = obj.user.email
        if info_email:
            return info_email
        return "Unknown"

    @admin.display(description="Phone Number")
    def user_phone_number(self, obj):
        info_phone_number = obj.user.phone_number
        if info_phone_number:
            return info_phone_number
        return "Unknown"

    @admin.display(description="Instagram url")
    def instagram(self, obj):
        if obj.instagram_url:
            return format_html('<a href="{}" target="_blank">Instagram url</a>', obj.instagram_url)
        return "No instagram url"
