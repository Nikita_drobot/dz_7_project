from django.urls import path

from main.views import (
    IndexView,
    UserLogin,
    UserLogout,
    UserRegistration,
    SendEmailView,
    ActivateUserView,
    ProfileData,
)

app_name = "main"

urlpatterns = [
    path("index/", IndexView.as_view(), name="index"),
    path("login/", UserLogin.as_view(), name="login"),
    path("logout/", UserLogout.as_view(), name="logout"),
    path("registration/", UserRegistration.as_view(), name="registration"),
    path("send-email/", SendEmailView.as_view(), name="send_email"),
    path(
        "activate/<str:uuid64>/<str:token>/",
        ActivateUserView.as_view(),
        name="active_user",
    ),
    path("profile/", ProfileData.as_view(), name="profile"),
]
