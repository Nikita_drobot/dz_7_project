from django.core.exceptions import ValidationError


def subject_validator(subject: str) -> None:
    list_subjects = ["math", "chemistry", "music", "physics", "history", "it"]
    if subject.lower() not in list_subjects:
        raise ValidationError(f"The subject must be from this list:\n{list_subjects}")
