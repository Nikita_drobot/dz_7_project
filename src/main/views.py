import time

from django.conf import settings
from django.contrib.auth import get_user_model, login
from django.contrib.auth.views import LoginView, LogoutView
from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views import View
from django.views.generic import TemplateView, CreateView, RedirectView

from main.forms import UserRegistrationForm
from main.models import UserProfile
from main.services.emails import send_registration_email
from main.utils.token_generator import TokenGenerator


class IndexView(TemplateView):
    template_name = "index.html"
    extra_context = {"site_name": "Hillel"}


class UserLogin(LoginView):
    ...


class UserLogout(LogoutView):
    ...


class UserRegistration(CreateView):
    template_name = "registration/create_user.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(request=self.request, user_instance=self.object)

        return super().form_valid(form)


class ActivateUserView(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data!!!")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)

            return super().get(request, *args, **kwargs)
        return HttpResponse("Wrong data!!!")


class SendEmailView(View):
    def get(self, request, *args, **kwargs):
        send_mail(
            subject="Hello from Hillel!",
            message=f"Mykyta Drobot, {time.strftime('%H:%M:%S', time.localtime())}",
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[
                settings.EMAIL_HOST_USER,
                "mikolaz2727@gmail.com",
            ],
            fail_silently=False,
        )

        return HttpResponse("Email has been sent!")


class ProfileData(View):
    def get(self, request):
        data = UserProfile.objects.get(user=request.user)
        return render(request, "users_data/profile.html", {"data": data})
